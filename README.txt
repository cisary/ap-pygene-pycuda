
Analytic programming using GPU for fitness calculation (interpreting mode)

- evolutionary technique: parallel genetic programming with migration of fittest organisms between populations and iterations

author: Michal Cisarik, cisary
		cis027@vsb.cz


Symbolic regression is implemented by pygene and pycuda to evolve arithmetic operations using evolutionary programming technique, nVidia CUDA is used for paralelization of fitness value calculation (interpreting arithmetical operations of a individual organism = expression) in every population co-evolving. Certain number of expressions are migrating between both populations and generations themselves. 
	
"Interpreting mode" - mapping populations to GPU and interpreting all its arithmetic operations

Instructions are coded as following example:

1) Instruction to interpreting (ins):

    Available instructions:
     
		- 0 parameters:
			'=' 000
		- 1 parameter:
	
2) Index of variable in temporary array to assign result (index):
	
3) Flag for choose variable (1) or constant (0) array

4) Index of constant or variable

instruction example:

ins | index | arg | i  |
000 |  001  |  1  | 01 |