#! /usr/bin/env python
"""

Analytic programming using GPU for fitness calculation (interpreting mode)

- evolutionary technique: parallel genetic programming with migration of fittest organisms between populations and iterations

author: Michal Cisarik, cisary
		cis027@vsb.cz

"""

# Python system modules:
import sys
import math
import time
import datetime
import StringIO
from re import sub
# Scipy
import numpy
from progressBar import *
from jinja2 import Template
#from pylab import *
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from random import random, uniform
from pygene.prog import ProgOrganism
from pygene.population import Population
from pygene.gene import FloatGene, FloatGeneMax
from pygene.organism import Organism, MendelOrganism

DEBUG, STATS = True

events,stats = {}


def benchmark(func):
    def wrapper(*args, **kwargs):
        t1 = time.time()
        results = func(*args, **kwargs)
        t2 = time.time()
        stats[str(func.func_name)]=numpy.float32((t2-t1)*1000.0)#str('%0.3f ms' % ((t2-t1)*1000.0))
        return results
    return wrapper

def duration():
    return float(sum(stats.values()))

from pycuda.compiler import SourceModule
import pycuda.driver as cuda

@benchmark
def pycuda_init():
    import pycuda.autoinit
pycuda_init()
if DEBUG:
    print(duration())

WIDTH = 64 # * int v poli instrukcii
ENDMARKER = 0 # koniec instrukcii

DFXMIN = -10
DFMAX = 10
DFSAMPLES = 64
POPULATIONS = 5

POPULATION = 32 # pri volani kernelu: grid=(POPULATION, 1)
EXPRESSIONS = 32

NEWORGANISMS = 40
CHILDORGANISMS = 32

EXPRESSIONSIZE = 16
BLOCKWIDTH = EXPRESSIONS
SHOWPROGRESS = 1 
BLOCKWIDTH = POPULATION
FITTEST = 8

COEVOLUTIONS = 10
MIGRATIONS = 6
CHILDREN = 11

(free,total) = cuda.mem_get_info()

print("\nGPU memory occupancy: %i%% free" % int(free * 100/ total))
print("_____________________________________________________________")
print("SYMBOLIC REGRESSION by pygene + pycuda in \"Interpreting mode\"")
print("_____________________________________________________________\n")

FUNCS=['+','-','*','/','**','sqrt','log','sin','cos','tan','=']
print("Available functions: "+str(FUNCS)+"\n")

VARS = [ 'x', 'y' ]
print("Available variables: "+str(VARS)+"\n")

CONSTS = [ 7.0, 1.0, 3.0, 10.0 ]
print("Available constants: "+str(CONSTS)+"\n")
       
TESTFUNC = "cos(3.0*x)"
print("Test function: "+TESTFUNC+"\n")

MODULE="numpy" # | "math" 

TESTFUNC = sub('sqrt', lambda x:MODULE+".sqrt", TESTFUNC)
TESTFUNC = sub('log', lambda x:MODULE+".log", TESTFUNC)
TESTFUNC = sub('sin', lambda x:MODULE+".sin", TESTFUNC)
TESTFUNC = sub('cos', lambda x:MODULE+".cos", TESTFUNC)
TESTFUNC = sub('tan', lambda x:MODULE+".tan", TESTFUNC)

TESTVALS_SIZE = 64
TESTVALS = [{'x':uniform(-10.0, 10.0),
             'y':uniform(-10.0, 10.0),
            } \
                for i in xrange(TESTVALS_SIZE)
           ]

class Expression:
    mem_size = 8 + numpy.intp(0).nbytes
    
    def __init__(self, array, struct_arr_ptr):
        self.ptr = struct_arr_ptr
        self.data = cuda.to_device(array)
        self.shape, self.dtype = array.shape, array.dtype
        
        cuda.memcpy_htod(int(struct_arr_ptr), numpy.float32(0.0))
        cuda.memcpy_htod(int(struct_arr_ptr) + 8, numpy.intp(int(self.data)))

    def __str__(self):
        return str(cuda.from_device(self.data, self.shape, self.dtype))
        
    def update(self,array):
        x=numpy.intp()
        ax = numpy.empty_like(x)
        cuda.memcpy_dtoh(ax, int(self.ptr) + 8)
        for i in xrange(0,len(array)):
            cuda.memcpy_htod(int(ax) + (4 * i), numpy.int32(array[i]))
            
        cuda.memcpy_htod(int(ax) + (4 * len(array)), numpy.int32(ENDMARKER))
        
    def fitness(self):
        x=numpy.float32()
        ax = numpy.empty_like(x)
        cuda.memcpy_dtoh(ax, self.ptr)
        return float(ax)
        
    def free(self):
        self.data.free()

class CUDA:
    def __init__(self,testvals):
        cu = open('kernel.cu', 'r').read()
        tpl = Template(cu)
        
        expected = []
        for testval in testvals:
            x=testval['x']
            y=testval['y']
            expected.append(numpy.float32(eval(TESTFUNC)))
        
        rendered_tpl = tpl.render(blockwidth = BLOCKWIDTH,
                                  testvals = testvals,
                                  expected = expected)
                                  
        #print("Compiling c++ source file \"kernel.cu\"\n")
        compiled_source = SourceModule(rendered_tpl, no_extern_c = 1)
        #print(rendered_tpl)
        self.kernel = compiled_source.get_function("eval")
        self.expressions=[]
        self.structs = cuda.mem_alloc( int(POPULATIONS * POPULATION) * Expression.mem_size)
        #print("Alocating memory (" + str(int(POPULATIONS * POPULATION) * Expression.mem_size) + " bytes)\n")
        
        for i in xrange(0,int(POPULATIONS * POPULATION)):
            ptr = int(self.structs) + (Expression.mem_size * i)
            self.expressions.append(Expression(numpy.zeros(shape = (1, WIDTH), dtype = numpy.int32), ptr))

    def __str__(self):
	    debug="___________________________________________________________________________\n"
	    for expression in self.expressions:
	        debug+=str(expression) + "\n"
	    return debug
    

    def hostToDevice(self,expressions_arrays):
        #print("Copying instructions of all expressions to GPU memory\n")
        i = 0
        for expression in self.expressions:
            if i<len(expressions_arrays):
                if len(expressions_arrays[i]) > 0:
                    expression.update(expressions_arrays[i])
            i+=1

    def freeDevice(self):
        #print("Freeing GPU memory")
        for expression in self.expressions:
            expression.free()

    def callKernel(self):
        #print("Executing CUDA kernel: eval(Expression* expressions)\")\n")
        #print(" block = (" + str(TESTVALS_SIZE) + ", 1, 1)")
        #print(" grid = (" + str(POPULATIONS * POPULATION) + ", 1)\n")
        
        self.kernel(self.structs,block = (TESTVALS_SIZE, 1, 1), grid=(POPULATIONS * POPULATION, 1))
        
    def deviceToHost(self):
        #print("Fetching fitness values from GPU")
        fitnesses=[]
        for expression in self.expressions:
            fitnesses.append(expression.fitness())
        return fitnesses

# define the class comprising the program organism
class MyProg(ProgOrganism):
    funcs = {
        '+': add,
        '-': sub,
        '*': mul,
        '/': div,
        '**': pow,
#       'sqrt': sqrt,
#       'log' : log,
        'sin' : sin,
        'cos' : cos,
#       'tan' : tan,
        }
        
    vars = VARS#['x', 'y']
    
    consts = CONSTS#[0.0, 1.0, 3.0, 10.0]

    testVals = []#TESTVALS
                
    #print(testVals)
 
    mutProb = 0.4
    
    def init(self,testvals):
        self.testVals=testvals
    
    def testFunc(self, **vars):
        return math.cos(3.0*vars['x'])

    def fitness(self):
        # choose 10 random values
        print("!!!!!!"+str(len(self.testVals)))
        badness = 0.0
        try:
            for vars in self.testVals:
                badness += (self.calc(**vars) - self.testFunc(**vars)) ** 2
            return badness
        except OverflowError:
            return 1.0e+255 # infinitely bad
        
    # maximum tree depth when generating randomly
    initDepth = 6

# now create the population class
class ProgPop(Population):
    species = MyProg
    initPopulation = POPULATION
    
    # cull to this many children after each generation
    childCull = POPULATION

    # number of children to create after each generation
    childCount = CHILDREN

    mutants = 0.3
 
        
class ParallelCoevolution:
    def __init__(self,testvals,migrating):
        self.cuda = CUDA(testvals)
        self.iteration = 0
        self.populations = [ ProgPop() for i in xrange(POPULATIONS) ]
        self.migrating = migrating #[ MyProg() for i in range(0, MIGRATIONS) ]
        self.sortedorganisms = {}
        self.testvals=testvals
        
        for organism in self.migrating:
            organism.init(self.testvals)    
        
    def toGPU(self):
        for population in self.populations: # Migration between iterations:
            pop=ProgPop()
            for i in xrange(0,MIGRATIONS):
                pop.add(self.migrating[-i])
            population=pop

        instructions_arrays=[]
        for population in self.populations: # Generating data for GPU memory - atithmetic instructions
            for organism in population:
                organism.init(self.testvals)
                instructions_arrays.append(organism.instructions())
        
        self.cuda.hostToDevice(instructions_arrays) # Map all organisms to GPU
        
    def executeGPU(self):
        self.cuda.callKernel()
#        pycuda.driver.Context.synchronize()
        self.fitnesses=self.cuda.deviceToHost()
        
        i = 0
        sortedorganisms = {}
        for population in self.populations:
            for organism in population:
                sortedorganisms[organism]=organism.fitness()#self.fitnesses[i]
                i+=1
                
        self.sortedorganisms=sorted(sortedorganisms, key = sortedorganisms.__getitem__, reverse = False)

        #print("len(self.sortedorganisms): " + str(len(self.sortedorganisms)))
        
    def migrate(self):
        self.migrated = []
  
        for i in xrange(0,MIGRATIONS):
            self.migrating.append(self.sortedorganisms[i])
        
        y = 0
        for i in xrange(0,POPULATIONS):
            temp=[]
            
            for i in xrange(0,MIGRATIONS):
                temp.append(self.migrating[-i])

            for j in xrange(0,POPULATION - MIGRATIONS):
                temp.append(self.sortedorganisms[y])
                y+=1
            self.migrated.append(temp)
            
    def fittest(self):
        out=[]
        self.sortedorganisms=sorted(self.sortedorganisms, key = sortedorganisms.__getitem__, reverse = False)
        for i in xrange(0,MIGRATIONS):
            out.append(self.sortedorganisms[i])
        return out
        
    def evolve(self):
        for population in self.populations:
            population.gen()
        self.iteration+=1

    def solution(self):
        for best in self.migrating:
            if best.fitness() <= 0:
                return True
        return False
        
    def __str__(self):
        print("Best solutions found:\n")
        
        for i in xrange(0,MIGRATIONS):
            best=self.sortedorganisms[i]
            print(str(i+1)+".)")
            best.dump()
            print("Fitness: "+str(best.fitness())+"\n_______________________")
            
        y=1
        print "Generation %s:" % (
            self.iteration
        )
        for population in self.populations:
            print "Population %s average = %s" % (
             y, population.fitness())
            y+=1
            """
        print "Migrating %s:" % (
            self.migrating
        )
        """
        return str(self.migrating[-1].instructions())
            
    def plot(self):
        imgwidth = 1000

        #majorLocator   = MultipleLocator(0x400)   # mark every 1024 bytes
        majorLocator   = MultipleLocator(100)
        majorFormatter = FormatStrFormatter('%d') # X change to %d to see decimal offsets
        left, width = .25, .50
        bottom, height = .25, .50
        right = left + width
        top = bottom + height

        ax = subplot(111)
        plot(results, linewidth=2.0, antialiased=True)
        subplots_adjust(left=0.02, right=0.99, bottom=0.2)

        ax.axis([0,filesize,0,8])
        ax.xaxis.set_major_locator(majorLocator)
        ax.xaxis.set_major_formatter(majorFormatter)
        xticks(rotation=315)
        
        xlabel('block (byte offset)')
        ylabel('entropy')
        title('Entropy levels')
        ax.text(0.99, 0.09, 'Average entropy: '+str(average),
                horizontalalignment='right',
                verticalalignment='bottom',
        #       backgroundcolor='b',
                weight='bold',
        #       color='w',
                alpha=0.8,
                backgroundcolor='w',
                color="b",
                transform=ax.transAxes)
        grid(True)
        
        img = gcf()
        img.set_size_inches(imgwidth, 6)
        img.savefig(self.directory+"uploads/entropy/output.png", dpi=imgdpi)

    def stats(self):
	    pass
    
    def dump(self,directory="stats/"):
        self.directory=directory
        self.plot()
        self.stats()
        self.cuda.free()
    
    
class CvGene(FloatGeneMax):
    """
    Gene which represents the numbers used in our organism
    """
    randMin = -100.0 # Genes get randomly generated within this range
    randMax = 100.0
    mutProb = 0.1 # Probability of mutation
    mutAmt = 0.1 # degree of mutation
    
    
testvals_genome = {}
for i in range(TESTVALS_SIZE * 2):
    testvals_genome[str(i)] = CvGene

class Migrations:
    def __init__(self):
        self.migrating = [ MyProg() for i in range(0, MIGRATIONS) ]
    
    def new(self,migrating):
        for o in migrating:
            self.migrating.append(o)

    def best(self):
        sortedorganisms = {}
        
        #print(self.migrating)
        for organism in self.migrating:
            sortedorganisms[organism]=organism.fitness
                
        migrating=sorted(sortedorganisms, key = sortedorganisms.__getitem__, reverse = False)
        out=[]
        for i in xrange(0,MIGRATIONS):
            out.append(migrating[i])
        return out
        

class Converger(MendelOrganism):
    """
    Implements the organism which tries
    to converge a function
    """
    
    genome = testvals_genome
    fit = 0.0
    duration = 0.0
   
    @benchmark
    def evolve(self, migrations):
        testvals = []
        i = 0
        while i < TESTVALS_SIZE * 2:
            testvals.append({'x':self[str(i)],'y':self[str(i+1)]})
            i+=2
            
        evolution = ParallelCoevolution(testvals,migrations.best())
        
        for i in xrange(0,10):
        
            evolution.toGPU()
            evolution.executeGPU()
            evolution.migrate()
            evolution.evolve()
            str(evolution)
            if evolution.solution():
                return True
            
        out = evolution.fittest()
        migrations.new(out)
        
        self.fit = out[0].fitness()
        return False
    
    def fitness(self):
        self.duration = duration()
        return float(self.fit * self.duration )
        
  
  
def main():

    converger = Population(species = Converger, initPopulation = 4, childCount = 2, childCull = 4)
    migrations = Migrations()
    
    try:
        while True:
            i=0
            for evolution in converger:
                stats={}
                if evolution.evolve(migrations):
                    break
            fittest=migrations.best()
            for best in fittest:
                print(str(best.fitness()))
            converger.gen(nfittest=10, nchildren=2)

    except KeyboardInterrupt:
        pass
    print("BEST:")
    
    best=migrations.best()
    
    for solution in best:
        print("__________________")
        solution.dump()
        print(solution.fitness())
    """
    evolution = ParallelCoevolution()
    
    found = False
    durations = []
    
    try:
        while not evolution.solution():
        
            evolution.toGPU()
            evolution.executeGPU()
            evolution.migrate()
            evolution.evolve()
            print(str(evolution))
            
    except KeyboardInterrupt:
        found = False
        
    evolution.dump()
            
    return found, durations
    """
def experiment(name):
    found, durations = main()
    return found

if __name__ == '__main__':
    main()
    print(stats)

"""
    if experiment(name):
        print("Solution found")
"""